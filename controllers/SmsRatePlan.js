'use strict';

var utils = require('../utils/writer.js');
var SmsRatePlan = require('../service/SmsRatePlanService');

module.exports.getsms_rate_plan = function getsms_rate_plan (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsRatePlan.getsms_rate_plan(contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_rate_plan = function sms_rate_plan (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsRatePlan.sms_rate_plan(contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
