'use strict';

var utils = require('../utils/writer.js');
var Product = require('../service/ProductService');

module.exports.product = function product (req, res, next) {
    var contentType = req.swagger.params['Content-Type'].value;
    var acc_id = req.swagger.params['acc_id'].value;
    var car_id = req.swagger.params['car_id'].value;
    var direction = req.swagger.params['direction'].value;
    var first_rec = req.swagger.params['first_rec'].value;
    var notes = req.swagger.params['notes'].value;
    var orderby_clause = req.swagger.params['orderby_clause'].value;
    var rec_count = req.swagger.params['rec_count'].value;
    var with_sms_rates = req.swagger.params['with_sms_rates'].value;
    Product.product(contentType,acc_id,car_id,direction,first_rec,notes,orderby_clause,rec_count,with_sms_rates)
        .then(function (response) {
            utils.writeJson(res, response);
        })
        .catch(function (response) {
            utils.writeJson(res, response);
        });
};