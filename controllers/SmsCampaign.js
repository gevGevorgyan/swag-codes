'use strict';

var utils = require('../utils/writer.js');
var SmsCampaign = require('../service/SmsCampaignService');

module.exports.deletesms_campaign = function deletesms_campaign (req, res, next) {
  var id = req.swagger.params['id'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsCampaign.deletesms_campaign(id,contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getsms_campaign = function getsms_campaign (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsCampaign.getsms_campaign(contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.putsms_campaign_put = function putsms_campaign_put (req, res, next) {
  var id = req.swagger.params['id'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsCampaign.putsms_campaign_put(id,contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_campaign = function sms_campaign (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsCampaign.sms_campaign(contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_campaign_get = function sms_campaign_get (req, res, next) {
  var id = req.swagger.params['id'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsCampaign.sms_campaign_get(id,contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
