'use strict';

var utils = require('../utils/writer.js');
var SmsPackSubscr = require('../service/SmsPackSubscrService');

module.exports.getsms_pack_subscr = function getsms_pack_subscr (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsPackSubscr.getsms_pack_subscr(contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.putsms_pack_subscr_put = function putsms_pack_subscr_put (req, res, next) {
  var id = req.swagger.params['id'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsPackSubscr.putsms_pack_subscr_put(id,contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_pack_subscr = function sms_pack_subscr (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsPackSubscr.sms_pack_subscr(contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_pack_subscr_get = function sms_pack_subscr_get (req, res, next) {
  var id = req.swagger.params['id'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsPackSubscr.sms_pack_subscr_get(id,contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
