'use strict';

var utils = require('../utils/writer.js');
var SmsSender = require('../service/SmsSenderService');

module.exports.deletesms_sender_delete = function deletesms_sender_delete (req, res, next) {
  var id = req.swagger.params['id'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsSender.deletesms_sender_delete(id,contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.postsms_sender = function postsms_sender (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsSender.postsms_sender(contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.putsms_sender_put = function putsms_sender_put (req, res, next) {
  var id = req.swagger.params['id'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsSender.putsms_sender_put(id,contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_sender = function sms_sender (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsSender.sms_sender(contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_sender_get = function sms_sender_get (req, res, next) {
  var id = req.swagger.params['id'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsSender.sms_sender_get(id,contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
