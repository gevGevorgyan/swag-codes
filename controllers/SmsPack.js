'use strict';

var utils = require('../utils/writer.js');
var SmsPack = require('../service/SmsPackService');

module.exports.deletesms_pack_content = function deletesms_pack_content (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var id = req.swagger.params['id'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsPack.deletesms_pack_content(contentType,id,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.postsms_pack_content = function postsms_pack_content (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsPack.postsms_pack_content(contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.putsms_pack_content = function putsms_pack_content (req, res, next) {
  var id = req.swagger.params['id'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsPack.putsms_pack_content(id,contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_pack = function sms_pack (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsPack.sms_pack(contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_pack_content = function sms_pack_content (req, res, next) {
  var first_rec = req.swagger.params['first_rec'].value;
  var orderby_clause = req.swagger.params['orderby_clause'].value;
  var pack_id = req.swagger.params['pack_id'].value;
  var rec_count = req.swagger.params['rec_count'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsPack.sms_pack_content(first_rec,orderby_clause,pack_id,rec_count,contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_pack_content_get = function sms_pack_content_get (req, res, next) {
  var id = req.swagger.params['id'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  SmsPack.sms_pack_content_get(id,contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
