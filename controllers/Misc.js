'use strict';

var utils = require('../utils/writer.js');
var Misc = require('../service/MiscService');

module.exports.activate_user = function activate_user (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  Misc.activate_user(contentType,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.bulk_send_sms = function bulk_send_sms (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  Misc.bulk_send_sms(contentType,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.captcha = function captcha (req, res, next) {
  var authorization = req.swagger.params['Authorization'].value;
  Misc.captcha(authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.edr = function edr (req, res, next) {
  var start_date = req.swagger.params['start_date'].value;
  var end_date = req.swagger.params['end_date'].value;
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  Misc.edr(start_date,end_date,contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.register_user = function register_user (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var body = req.swagger.params['Body'].value;
  Misc.register_user(contentType,body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.send_sms = function send_sms (req, res, next) {
  var from = req.swagger.params['from'].value;
  var to = req.swagger.params['to'].value;
  var message = req.swagger.params['message'].value;
  var authorization = req.swagger.params['Authorization'].value;
  Misc.send_sms(from,to,message,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.sms_campaign_stats = function sms_campaign_stats (req, res, next) {
  var contentType = req.swagger.params['Content-Type'].value;
  var authorization = req.swagger.params['Authorization'].value;
  Misc.sms_campaign_stats(contentType,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
