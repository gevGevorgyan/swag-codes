'use strict';


/**
 * sms_sender
 *
 * id String 
 * contentType String 
 * authorization String  (optional)
 * returns sms_sender~1891
 **/
exports.deletesms_sender_delete = function(id,contentType,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "rows_affected" : 1
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * sms_sender
 *
 * contentType String 
 * body Sms_sender_request 
 * authorization String  (optional)
 * returns sms_sender1
 **/
exports.postsms_sender = function(contentType,body,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "id" : 89
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * sms_sender
 *
 * id String 
 * contentType String 
 * body Sms_sender_request 
 * authorization String  (optional)
 * returns sms_sender~1891
 **/
exports.putsms_sender_put = function(id,contentType,body,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "rows_affected" : 1
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * sms_sender
 *
 * contentType String 
 * authorization String  (optional)
 * returns List
 **/
exports.sms_sender = function(contentType,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "id" : 89,
  "name" : "Test"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * sms_sender
 *
 * id String 
 * contentType String 
 * authorization String  (optional)
 * returns sms_sender~189
 **/
exports.sms_sender_get = function(id,contentType,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "id" : 89,
  "name" : "Test"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

