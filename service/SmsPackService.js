'use strict';


/**
 * sms_pack_content
 *
 * contentType String 
 * id String 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.deletesms_pack_content = function(contentType,id,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_pack_content
 *
 * contentType String 
 * body Sms_pack_content_request 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.postsms_pack_content = function(contentType,body,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_pack_content
 *
 * id String 
 * contentType String 
 * body Sms_pack_content_request 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.putsms_pack_content = function(id,contentType,body,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_pack
 *
 * contentType String 
 * body Sms_pack_request 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.sms_pack = function(contentType,body,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_pack_content
 *
 * first_rec Integer 
 * orderby_clause Integer 
 * pack_id Integer 
 * rec_count Integer 
 * contentType String 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.sms_pack_content = function(first_rec,orderby_clause,pack_id,rec_count,contentType,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_pack_content
 *
 * id String 
 * contentType String 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.sms_pack_content_get = function(id,contentType,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

