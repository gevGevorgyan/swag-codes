'use strict';


/**
 * product
 * Method GET:product returns the list of available products. The Carriers permission must be granted to get the list. VPD restricted. Additional filters - such as car_id, acc_id, direction (0 - client, 1 - vendor), notes (case-sensitive, one value can be set) and with_sms_rates (0 - show all products, 1 - show products only with rates active at the moment) can be set.
 *
 * contentType String
 * acc_id String Account ID (optional)
 * car_id Integer Carrier ID (optional)
 * direction Integer Product direction: 0 - client, 1 - vendor (optional)
 * first_rec Integer Pagination: first record (optional)
 * notes String Product notes (optional)
 * orderby_clause String Sort expression: list of column numbers separated by comma (1 - id, 2 - acc_id, 3 - descr, 4 - parent_product_id, 5 - direction, 6 - type, 7 - check_jurisdiction, 8 - dip_for_lrn, 9 - billable, 10 - notes, 11 - rates_based_on, 12 - invoice_group_index, 13 - ignore_stats, 14 - poi_cnt, 15 - block_for_lrn_calls, 16 - ani_tag, 17 - ten_digit_calling_as_us, 18 - check_lata, 19 - reverse_charge, 20 - check_ani_tags, 21 - bill_delivered_only, 22 - billing_mode, 23 - base_product_id, 24 - is_active, 25 - bill_number_options, 26 - cache_lrn_period, 27 - def_indeterminate_price, 28 - hlr_prefix_list, 29 - block_end_date, 30 - block_type, 31 - sibling_product_id, 32 - exempt_from_held_duration, 33 - rate_inheritance_mode, 34 - use_sender_mccmnc_rates, 35 - car_id) (optional)
 * rec_count Integer Pagination: maximum number of records (optional)
 * with_sms_rates Integer Flag: 1 - filter products with sms rate, 0 - do not filter (optional)
 * no response value expected for this operation
 **/
exports.product = function(contentType,acc_id,car_id,direction,first_rec,notes,orderby_clause,rec_count,with_sms_rates) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}
