'use strict';


/**
 * activate_user
 *
 * contentType String 
 * body Activate_user_request 
 * no response value expected for this operation
 **/
exports.activate_user = function(contentType,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * bulk_send_sms
 *
 * contentType String 
 * body List 
 * authorization String  (optional)
 * returns bulk_send_sms
 **/
exports.bulk_send_sms = function(contentType,body,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "sentCount" : 0,
  "rejectedCount" : 3,
  "totalCount" : 3,
  "details" : [ {
    "message_id" : "98ba4269-b4d6-4370-8dd4-5d8ea8946305",
    "dnis" : 37499955509,
    "segment_num" : 1,
    "http_status" : 402
  }, {
    "message_id" : "5088c242-a599-44fb-976c-a10334b870d2",
    "dnis" : 37499955509,
    "segment_num" : 1,
    "http_status" : 402
  }, {
    "message_id" : "a29f02a2-9a2b-4d70-8fdf-64776e1f34e5",
    "dnis" : 37499955509,
    "segment_num" : 1,
    "http_status" : 402
  } ]
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * captcha
 *
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.captcha = function(authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * edr
 *
 * start_date String 
 * end_date String 
 * contentType String 
 * authorization String  (optional)
 * returns List
 **/
exports.edr = function(start_date,end_date,contentType,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "edr_id" : 1081025,
  "edr_date" : "2020.01.15 23:13:30",
  "edr_status" : "NOT ACCEPTED",
  "edr_is_successful" : 0,
  "message_id" : "a29f02a2-9a2b-4d70-8fdf-64776e1f34e5",
  "dnis" : 37499955509,
  "ani" : "test",
  "message_text" : "Test message",
  "sender_acc_id" : 11920,
  "poi_id" : 18944,
  "ext_info" : "Insufficient funds for account ID 11920, available funds including credit limit 0.022000000000000006, required 0.078",
  "reason_code" : 402,
  "segment_num" : 1,
  "mccmnc" : 283001,
  "rate" : 0.078
}, {
  "edr_id" : 1081024,
  "edr_date" : "2020.01.15 23:13:30",
  "edr_status" : "NOT ACCEPTED",
  "edr_is_successful" : 0,
  "message_id" : "5088c242-a599-44fb-976c-a10334b870d2",
  "dnis" : 37499955509,
  "ani" : "test",
  "message_text" : "Test message",
  "sender_acc_id" : 11920,
  "poi_id" : 18944,
  "ext_info" : "Insufficient funds for account ID 11920, available funds including credit limit 0.022000000000000006, required 0.078",
  "reason_code" : 402,
  "segment_num" : 1,
  "mccmnc" : 283001,
  "rate" : 0.078
}, {
  "edr_id" : 1081023,
  "edr_date" : "2020.01.15 23:13:30",
  "edr_status" : "NOT ACCEPTED",
  "edr_is_successful" : 0,
  "message_id" : "98ba4269-b4d6-4370-8dd4-5d8ea8946305",
  "dnis" : 37499955509,
  "ani" : "test",
  "message_text" : "Test message",
  "sender_acc_id" : 11920,
  "poi_id" : 18944,
  "ext_info" : "Insufficient funds for account ID 11920, available funds including credit limit 0.022000000000000006, required 0.078",
  "reason_code" : 402,
  "segment_num" : 1,
  "mccmnc" : 283001,
  "rate" : 0.078
}, {
  "edr_id" : 1081007,
  "edr_date" : "2020.01.15 23:08:41",
  "edr_status" : "NOT ACCEPTED",
  "edr_is_successful" : 0,
  "message_id" : "2a1e006a-3cc4-423e-b852-80548e1b6b10",
  "dnis" : 37499955509,
  "ani" : "test",
  "message_text" : "Test message",
  "sender_acc_id" : 11920,
  "poi_id" : 18944,
  "ext_info" : "Insufficient funds for account ID 11920, available funds including credit limit 0.022000000000000006, required 0.078",
  "reason_code" : 402,
  "segment_num" : 1,
  "mccmnc" : 283001,
  "rate" : 0.078
}, {
  "edr_id" : 1081006,
  "edr_date" : "2020.01.15 23:08:41",
  "edr_status" : "NOT ACCEPTED",
  "edr_is_successful" : 0,
  "message_id" : "94e3002d-afd3-4e13-9eeb-b4240fb77289",
  "dnis" : 37499955509,
  "ani" : "test",
  "message_text" : "Test message",
  "sender_acc_id" : 11920,
  "poi_id" : 18944,
  "ext_info" : "Insufficient funds for account ID 11920, available funds including credit limit 0.022000000000000006, required 0.078",
  "reason_code" : 402,
  "segment_num" : 1,
  "mccmnc" : 283001,
  "rate" : 0.078
}, {
  "edr_id" : 1081005,
  "edr_date" : "2020.01.15 23:08:41",
  "edr_status" : "NOT ACCEPTED",
  "edr_is_successful" : 0,
  "message_id" : "9df46323-db45-410f-9c40-a249822dcdfa",
  "dnis" : 37499955509,
  "ani" : "test",
  "message_text" : "Test message",
  "sender_acc_id" : 11920,
  "poi_id" : 18944,
  "ext_info" : "Insufficient funds for account ID 11920, available funds including credit limit 0.022000000000000006, required 0.078",
  "reason_code" : 402,
  "segment_num" : 1,
  "mccmnc" : 283001,
  "rate" : 0.078
}, {
  "edr_id" : 1080959,
  "edr_date" : "2020.01.15 22:36:31",
  "edr_status" : "DELIVRD",
  "edr_is_successful" : 1,
  "message_id" : "5e033e9b-8463-05d7-ab03-97f7a7e8362d",
  "dnis" : 37499955509,
  "ani" : "Test",
  "message_text" : "test message",
  "sender_acc_id" : 11920,
  "poi_id" : 18944,
  "reason_code" : 200,
  "segment_num" : 1,
  "mccmnc" : 283001,
  "rate" : 0.078
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * register_user
 *
 * contentType String 
 * body Register_user_request 
 * no response value expected for this operation
 **/
exports.register_user = function(contentType,body) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * send_sms
 *
 * from String 
 * to String 
 * message String 
 * authorization String  (optional)
 * returns success
 **/
exports.send_sms = function(from,to,message,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "message_id" : "5e033e9b-8463-05d7-ab03-97f7a7e8362d",
  "message_text" : "dGVzdCBtZXNzYWdl"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * sms_campaign_stats
 *
 * contentType String 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.sms_campaign_stats = function(contentType,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

