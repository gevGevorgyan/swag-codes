'use strict';


/**
 * sms_campaign
 *
 * id String 
 * contentType String 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.deletesms_campaign = function(id,contentType,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_campaign
 *
 * contentType String 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.getsms_campaign = function(contentType,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_campaign
 *
 * id String 
 * contentType String 
 * body Sms_campaign_request 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.putsms_campaign_put = function(id,contentType,body,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_campaign
 *
 * contentType String 
 * body Sms_campaign_request 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.sms_campaign = function(contentType,body,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_campaign
 *
 * id String 
 * contentType String 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.sms_campaign_get = function(id,contentType,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

