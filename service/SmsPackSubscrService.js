'use strict';


/**
 * sms_pack_subscr
 *
 * contentType String 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.getsms_pack_subscr = function(contentType,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_pack_subscr
 *
 * id String 
 * contentType String 
 * body Sms_pack_subscr~11_request 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.putsms_pack_subscr_put = function(id,contentType,body,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_pack_subscr
 *
 * contentType String 
 * body Sms_pack_subscr_request 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.sms_pack_subscr = function(contentType,body,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * sms_pack_subscr
 *
 * id String 
 * contentType String 
 * authorization String  (optional)
 * no response value expected for this operation
 **/
exports.sms_pack_subscr_get = function(id,contentType,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}

